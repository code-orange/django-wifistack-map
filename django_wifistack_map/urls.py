from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

from django_wifistack_map_main.django_wifistack_map_main import urls

urlpatterns = [path("", include(urls)), path("admin/", admin.site.urls)] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
